/**
 * Created by filchakov on 6/24/2016.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.*;
import java.util.ArrayList;

public class LAbAPI {

    public boolean Response(String urlnew, String expected) throws IOException {
            boolean isContains = false;
            try {
                URL url = new URL(urlnew);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");


                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("HTTP error code:"
                            + conn.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
                while ((output = br.readLine()) != null) {
                    if (output.contains(expected)) {
                        isContains = true;
                    }
                    System.out.println(output);
                }

            } catch (NullPointerException ex) {
                System.out.println("Select any other folder");
            }
            return isContains;
        }

        public ArrayList<String> readFile(String path) throws IOException{
            ArrayList<String> lines = new ArrayList<>();
            String line;
            try (BufferedReader br = new BufferedReader(new FileReader(path))) {
                while((line = br.readLine()) != null){
                    lines.add(line);
                }
            } catch (FileNotFoundException e) {
                throw  new IOException(e.getMessage());
            } catch (IOException e) {
                throw  new IOException(e.getMessage());
            }
            return  lines;
        }
    }


