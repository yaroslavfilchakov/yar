/**
 * Created by filchakov on 6/24/2016.
 */import org.junit.*;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;

public class TestAPI {
    LAbAPI ClassNew = new LAbAPI();
    String URL = "./URLS.txt";

    @Test
    public void checkApiCallsByFile() throws IOException {
        List<String> inputData = ClassNew.readFile(URL);
        for (int i = 0; i < inputData.size(); i++) {
            String[] splitedData = inputData.get(i).split("   ");
            boolean isContains = ClassNew.Response(splitedData[0], splitedData[1]);
            if (isContains)
                assertTrue("called Api method '" + splitedData[0] + "'contains data: " + splitedData[1] + "'",
                        isContains);
            else assertFalse("called Api method '" + splitedData[0] + "'doesn't contain data: " + splitedData[1] + "'",
                    isContains);

        }
    }
}
