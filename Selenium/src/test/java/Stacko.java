import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by фильчаковы on 30.05.2016.
 */
public class Stacko {
    private WebDriver driver;
    private String baseUr2;
    String Actualtext;
    int feat=0;

    public Stacko() {
    }

    @Before
    public void setUp() throws Exception {
        this.driver = new FirefoxDriver();
        this.baseUr2 = "http://stackoverflow.com/";
    }

    @Test
    public void testGoogle() throws InterruptedException {
        this.driver.get(this.baseUr2);
        this.driver.manage().window().maximize();
        this.driver.manage().timeouts().implicitlyWait(15L, TimeUnit.SECONDS);

        String elementval = driver.findElement(By.xpath(".//*[@id='tabs']/a[2]/span")).getText();
        feat=Integer.parseInt(elementval);
        Assert.assertTrue("menwe 300",feat>300);


        this.driver.findElement(By.xpath(".//span/a[text()=\'sign up\']")).click();
        Assert.assertTrue("Google button is not displayed", this.driver.findElement(By.xpath(".//*[@id=\'openid-buttons\']/div/div/span[contains(text(),\'Google\')]/../../div")).isDisplayed());

        this.driver.findElement(By.xpath(".//span/a[text()=\'sign up\']")).click();
        Assert.assertTrue("Facebook button is not displayed", this.driver.findElement(By.xpath(".//*[@id=\'openid-buttons\']/div/div/span[contains(text(),\'Facebook\')]/../../div")).isDisplayed());

        this.driver.findElement(By.xpath(".//*[@id=\'hlogo\']/a")).click();

        this.driver.findElement(By.xpath(".//div[2]/h3/a")).click();
        Assert.assertTrue("NOT Today", this.driver.findElement(By.xpath(".//tbody/tr/td/p/b[text()=\'today\']")).isDisplayed());



    }

    @After
    public void tearDown() throws Exception {
        this.driver.quit();
    }
}
