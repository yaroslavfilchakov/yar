import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by filchakov on 5/30/2016.
 */
public class rozetka {

    private WebDriver driver;
    private java.lang.String baseUr1;

    @Before
    public void setUp() throws Exception{
        driver = new FirefoxDriver();
        baseUr1 = "http://rozetka.com.ua";
/*driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);*/
    }
    @Test
    public void testGoogle() throws InterruptedException{
        driver.get(baseUr1);
        driver.manage().window().maximize();
        Assert.assertTrue("logo is not displayed",
                driver.findElement(By.xpath(".//div/img")).isDisplayed());
        Assert.assertTrue("Apple is not displayed",
                driver.findElement(By.xpath(".//li/a[contains(text(),'Apple')]")).isDisplayed());
        Assert.assertTrue("MP3 is not displayed",
                driver.findElement(By.xpath(".//*[@id='m-main']/li[3]/a[contains(text(),'MP3')]")).isDisplayed());
        driver.findElement(By.xpath(".//a/span")).click();
        Assert.assertTrue("Goroda is not displayed",
                driver.findElement(By.xpath(".//div[1]/div/div[2]/div/div/div/a[contains(text(),'Харьков') or contains(text(),'Киев') or contains(text(),'Одесса')]")).isDisplayed());
        driver.findElement(By.xpath(".//a[@class=\"sprite-side novisited hub-i-link hub-i-cart-link\"]")).click();
        Assert.assertTrue("karzina is not displayed",
                driver.findElement(By.xpath(".//h2[@class=\'empty-cart-title inline sprite-side\']")).isDisplayed());

        TimeUnit.SECONDS.sleep(6);


    }
    @After
    public void tearDown() throws Exception{
        driver.quit();
    }
}
