/**
 * Created by filchakov on 4/11/2016.
 */
public class b1 {
    public static void main(String[] args)
    {
        int a = 12;
        String str=Integer.toString(a);
        System.out.println(str);

        try {
            Integer a1 = Integer.valueOf(str);
            System.out.println(a1);
        }catch (NumberFormatException e ) {
            System.err.println("Неверный формат строки!");
        }

        double b=12.25698;
        String str2=Double.toString(b);
        System.out.println(str2);

        try {
            Double b1 = Double.valueOf(str2);
            System.out.println(b1);
        }catch (NumberFormatException e ) {
            System.err.println("Неверный формат строки!");
        }


        long c=56788888;
        String str3=Long.toString(c);
        System.out.println(str3);

        try {
            Long c1 = Long.valueOf(str3);
            System.out.println(c1);
        }catch (NumberFormatException e ) {
            System.err.println("Неверный формат строки!");
        }

        float d=8.65f;
        String str4=Float.toString(d);
        System.out.println(str4);

        try {
            Float d1 = Float.valueOf(str4);
            System.out.println(d1);
        }catch (NumberFormatException e ) {
            System.err.println("Неверный формат строки!");
        }

        char t='9';
        String str5= Character.toString(t);
        System.out.println(str5);

       char t1=str5.charAt(0);
        System.out.println(t1);



        byte f=10;
        String str6=Byte.toString(f);
        System.out.println(str6);
        try {
            Byte f1 = Byte.valueOf(str6);
            System.out.println(f1);
        }catch (NumberFormatException e ) {
            System.err.println("Неверный формат строки!");
        }
        short g=4564;
        String str7=Short.toString(g);
        System.out.println(str7);
        try {
            Short g1 = Short.valueOf(str7);
            System.out.println(g1);
        }catch (NumberFormatException e ) {
            System.err.println("Неверный формат строки!");
        }

        boolean bool1;
        bool1=true;
        String str8=Boolean.toString(bool1);
        System.out.println(str8);
        try {
            Boolean bool2 = Boolean.valueOf(str8);
            System.out.println(bool2);
        }catch (NumberFormatException e ) {
            System.err.println("Неверный формат строки!");
        }


    }
}
