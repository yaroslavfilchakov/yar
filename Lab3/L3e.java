import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by filchakov on 4/15/2016.
 */
public class L3e {
    /*
    Вводить с клавиатуры числа и считать их сумму, пока пользователь не введёт слово «сумма».
    Вывести на экран полученную сумму.*/
    public static void main(String[] args) throws Exception {


            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String s = reader.readLine();
            int x = Integer.parseInt(s);
            int sum = 0;
            //Boolean exit = false;

            while (true) {
                sum = sum + x;
                s = reader.readLine();
                if (s.equals("сумма"))
                    break;
                else
                    x = Integer.parseInt(s);

            }
            System.out.println(sum);
        }
    }
