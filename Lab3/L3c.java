import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.System.in;

/**
 * Created by filchakov on 4/13/2016.
 */
public class L3c {
    public static void main(String[] args) throws IOException {
        //Вывести все простые числа от 1 до N. N задается пользователем через консоль
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Введите целое число: ");
                int m=Integer.valueOf(reader.readLine());


                int numbers = 0;
                for(int i = 2; i<m; i++)
                {
                    boolean prime = true;
                    for(int j = 2; j < i; j++)
                    {
                        if(i % j == 0)
                            prime = false;
                    }
                    if(prime) {
                        System.out.println(i);

                    }
                }

            }
        }




