/**
 * Created by filchakov on 4/13/2016.
 */
public class L3b {
    public static void main(String[] args){
        // При помощи цикла for вывести на экран нечетные числа от 1 до 99.
        int a[]=new int[100];
        for (int i=1; i<a.length;  i++, i++) {
            a[i]=i;
            int b=i;
            System.out.println(b);
        }

    }
}
