package Lab4;

/**
 * Created by filchakov on 4/21/2016.
 */
public class Car2 extends Cars{
    public static String name(){
        String name="Car2";
        return name;
    }
    public Car2(double v,double a, double Vpovorot,double V0){
        super(v, a, Vpovorot,V0);
    }
    @Override
    public void turn(){
        if(((v-V0)/a)<v/2){
            Vpovorot=Math.abs((v-V0)/2*a);
        }
    }
}