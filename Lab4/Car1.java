package Lab4;

/**
 * Created by filchakov on 4/21/2016.
 */
public class Car1 extends Cars{
    public static String name(){
        String name="Car1";
        return name;
    }
    public Car1(double v,double a, double Vpovorot, double V0){
        super(v, a, Vpovorot,V0);
    }
    @Override
    public void turn(){
        if(V0>v/2){
            Vpovorot=Math.abs(v/2-V0)*0.005;
        }
    }
}
