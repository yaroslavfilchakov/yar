package Lab4;

/**
 * Created by filchakov on 4/21/2016.
 */
public class Car3 extends Cars{
    public static String name(){
        String name="Car3";
        return name;
    }
    public Car3(double v,double a, double Vpovorot,double V0){
        super(v, a, Vpovorot,V0);
    }
    @Override
    public void turn(){
        if(V0==v){
            Vpovorot=Math.abs((v*0.1));
        }
    }
}