import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 09.06.2016.
 */
public class SignUp {
    private WebDriver driver;

    public SignUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//span/a[text()='sign up']")
    public WebElement link_signup;

    @FindBy(xpath = ".//*[@id='openid-buttons']/div/div/span[contains(text(),'Google')]/../../div ")
    public WebElement lnk_google;



    @FindBy(xpath = ".//*[@id='openid-buttons']/div/div/span[contains(text(),'Facebook')]/../../div")
        public WebElement lnk_menu_facebook;

    public StartPage navigateToSignUp() {
        link_signup.click();


        return  new StartPage(driver);
    }
}
