import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * Created by фильчаковы on 09.06.2016.
 */
public class Testing  {
        private static WebDriver driver;
        private static StartPage startpage;
        private static Featured featured;
        private static SignUp singup;
        private static TopQuestions topquestions;


        @BeforeClass
        public static void SetUp(){
            driver =new FirefoxDriver();
            driver.get("http://stackoverflow.com/");
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            startpage = new StartPage(driver);
            featured=new Featured(driver);
            singup=new SignUp(driver);
            topquestions=new TopQuestions(driver);
        }
        @AfterClass
        public  static void tearDown(){
            driver.close();
        }
        @Test
        public void TestLabelStartPage(){
            assertTrue("content is absent", startpage.label_stack.isDisplayed());
        }
        @Before
        public void Before1(){
            if(!driver.getCurrentUrl().equals("http://stackoverflow.com/"))
                driver.get("http://stackoverflow.com/");
        }

        @Test
        public void TestKoli4ectvaFeatured(){
        Integer kol=Integer.parseInt(String.valueOf(featured.link_featured.getText()));
        assertTrue("menwe 300", kol>300);
    }
        @Test
    public void TestSignUp() {
        startpage = singup.navigateToSignUp();
        assertTrue("Google is not displayed", singup.lnk_google.isDisplayed());
            assertTrue("Facebook is not displayed", singup.lnk_menu_facebook.isDisplayed());
    }
    @Test
    public void TestQuestionToday() {
        startpage = topquestions.navigateQuestion();
        assertTrue("Not Today", topquestions.text_today.isDisplayed());
    }


}