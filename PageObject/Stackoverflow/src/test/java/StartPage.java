import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 09.06.2016.
 */
public class StartPage {
    private WebDriver driver;

    public StartPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy(xpath = ".//div/a[contains(text(),'Stack Overflow')]")
    public WebElement label_stack;
}
