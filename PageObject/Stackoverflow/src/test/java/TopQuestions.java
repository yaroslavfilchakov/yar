import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 10.06.2016.
 */
public class TopQuestions {
    private WebDriver driver;

    public TopQuestions(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div[2]/h3/a")
    public WebElement link_anyquestion;

    @FindBy(xpath = ".//tbody/tr/td/p/b[text()='today']")
    public WebElement text_today;

    public StartPage navigateQuestion() {
        link_anyquestion.click();


        return  new StartPage(driver);
    }
}
