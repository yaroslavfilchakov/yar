import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 07.06.2016.
 */
public class KatalogTovarov {
    private WebDriver driver;

    public KatalogTovarov(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;

    }
    @FindBy(xpath = ".//li/a[contains(text(),'Apple')]")
    public WebElement apple_element;

    @FindBy(xpath = ".//*[@id='m-main']/li[3]/a[contains(text(),'MP3')]")
    public WebElement mp3_element;
}
