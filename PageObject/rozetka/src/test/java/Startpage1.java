import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by фильчаковы on 07.06.2016.
 */
public class Startpage1 {

    private WebDriver driver;

    public Startpage1(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy(xpath = ".//div/img")
    public WebElement label_rozetka;


}
