import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * Created by фильчаковы on 07.06.2016.
 */
public class Testing {
    private static WebDriver driver;
    private static Korzina1 korzina1;
    private static Startpage1 startpage1;
    private static KatalogTovarov katalogtovarov;
    private static Gorod gorod;

    @BeforeClass
    public static void SetUp(){
        driver =new FirefoxDriver();
        driver.get("http://rozetka.com.ua");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        startpage1=new Startpage1(driver);
        korzina1=new Korzina1(driver);
        katalogtovarov=new KatalogTovarov(driver);
        gorod=new Gorod(driver);
    }
    @AfterClass
    public  static void tearDown(){
        driver.close();
    }

    @Test
    public void TestLabelStartPage(){
        assertTrue("Label is missing", startpage1.label_rozetka.isDisplayed());
    }
    @Test
    public void TestKorzinaisEmpty(){
        startpage1=korzina1.navigateToCorzina();
        assertTrue("korzina is not displayed", korzina1.korzina_empty.isDisplayed());
    }
    @Test
    public void TestAppleLink(){
        assertTrue("apple not displayed", katalogtovarov.apple_element.isDisplayed());
    }
    @Test
    public void TestMP3Link(){
        assertTrue("MP3 is not displayed", katalogtovarov.mp3_element.isDisplayed());
    }
    @Test
    public void TestGoroda() {
        startpage1 = gorod.navigateToGoroda();
        assertTrue("gorodov net", gorod.tri_goroda.isDisplayed());
    }
}
