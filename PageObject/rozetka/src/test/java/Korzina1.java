import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 07.06.2016.
 */
public class Korzina1 {
    private WebDriver driver;

    public Korzina1(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    public WebElement lnk_menu_korzina;

    @FindBy(xpath = ".//h2[@class='empty-cart-title inline sprite-side']")
    public WebElement korzina_empty;

    public Startpage1 navigateToCorzina() {
        lnk_menu_korzina.click();


        return  new Startpage1(driver);
    }
}
