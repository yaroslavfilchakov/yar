import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by фильчаковы on 07.06.2016.
 */
public class Gorod {
    private WebDriver driver;

    public Gorod(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    @FindBy(xpath = ".//*[@id='city-chooser']/div[1]/div/div[2]/div/div/div/a[contains(text(),'Харьков') or contains(text(),'Киев') or contains(text(),'Одесса')]")
    public WebElement tri_goroda;

    @FindBy(xpath = ".//*[@id='city-chooser']/a")
    public WebElement link_goroda;


    public Startpage1 navigateToGoroda() {
        link_goroda.click();

        return new Startpage1(driver);
    }
}
